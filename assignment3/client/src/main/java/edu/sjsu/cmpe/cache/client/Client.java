package edu.sjsu.cmpe.cache.client;

import com.google.common.hash.*;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


public class Client {
	
	private static SortedMap<Integer, String> hashing =new TreeMap<Integer, String>();
	private static HashFunction function= Hashing.md5();

	//  Rendezvous hashing
		 
		 /*public static void addCache(int addKey, String addValue, RendezvousHash hash){
	        String url = (String) hash.getCache(addKey);
	        CacheServiceInterface cache = new DistributedCacheService(url);
	        cache.put(addKey,addValue);
	        System.out.println("put( " + addKey + " => " + addValue + ")");
	    }
	    public static Object getRendCache(int key, RendezvousHash hash){
	        String url = (String) hash.getCache(key);
	        CacheServiceInterface cache = new DistributedCacheService(url);
	        String result = cache.get(key);
	        System.out.println("get( "+ key+ " ) => "+ result);
	        return result;
	    }*/

		 public static String get(Object key) {
		    if (hashing.isEmpty()) {
		      return null;
		    }
		    int hash = function.hashLong((Integer)key).asInt();
		    if (!hashing.containsKey(hash)) {
		      SortedMap<Integer, String> tailMap =hashing.tailMap(hash);
		      hash = tailMap.isEmpty() ?
		             hashing.firstKey() : tailMap.firstKey();
		    }
		    return hashing.get(hash);
		  }
	 
		 public static void add(String node,int i) {
			 HashCode hashCode=function.hashLong(i);
			  hashing.put(hashCode.asInt(),node);
		 }
	
		  public void remove(String node) {
			  hashing.remove(Hashing.md5().hashCode());
		  }
	 
	    public static void main(String[] args) throws Exception {

	        String value[]={"a","b","c","d","e","f","g","h","i","j"};

	        System.out.println("Starting Client...");

	        String server1="http://localhost:3000";
	        String server2="http://localhost:3001";
	        String server3="http://localhost:3002";

	        List<String> server = new ArrayList<String>();

	        server.add(server1);
	        server.add(server2);
	        server.add(server3);

	        for(int x=0; x<server.size(); x++)
	        {
	            String x1=server.get(x);
	            System.out.println("Get string " + x1);
	            add(x1,x);
	        }

	        for (int i=0;i<10;i++)
	        {
	            int bucket = Hashing.consistentHash(Hashing.md5().hashLong(i),hashing.size());
	            int key=i+1;
	            String latestServer=get(bucket);

	            System.out.println("Server :" + latestServer);

	            CacheServiceInterface cacheserver = new DistributedCacheService(latestServer);

	            cacheserver.put(i+1, String.valueOf(value[i]));
	            System.out.println("inserting:");
	            System.out.println("key :" + key);
	            System.out.println("Values are :" + value[i]);

	        }

	        for (int i=0;i<10;i++)
	        {
	            int bucket = Hashing.consistentHash(Hashing.md5().hashLong(i),hashing.size());
	            int key=i+1;
	            String latestServer=get(bucket);

	            CacheServiceInterface cacheserver = new DistributedCacheService(latestServer);

	            cacheserver.get(i+1);
	            System.out.println("getting");
	            System.out.println("key : " + key);
	            System.out.println("Values are : "+cacheserver.get(i+1));

	        }
	        
//	        Rendezvous hashing
	        
	/*      ArrayList cacheServer = new ArrayList();

	        cacheServer.add("http://localhost:3000");
	        cacheServer.add("http://localhost:3001");
	        cacheServer.add("http://localhost:3002");


	        RendezvousHash<String> hash = new RendezvousHash(cacheServer);
	        System.out.println("Adding to Cache");
	        for(int i = 1; i<=10; i++){
	        	addCache(i, String.valueOf((char) (i + 96)), hash);
	        }
	        System.out.println("Getting from Cache Server");
	        for(int i =1; i<=10; i++){
	            String value = (String)getRendCache(i, hash);
	            System.out.println("Cache : " + value);
	        }*/

	    }
}
